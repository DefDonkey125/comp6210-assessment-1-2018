<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="Assignment_1/css/basic.css">
    <title>Heathcare Center Place</title>
    </head>
    <body>

    		<section class="content-container">
        <header>
				<img class="logo" src="img/header.png" alt="Logo" >
				<span class="web-title">Heathcare center Place</span>
			</header>
			<nav>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li><a href="About_Us.html">About Us</a></li>
					<li><a href="Contact.html">Contact</a></li>
				</ul>
			</nav>
			<section>
      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="https://placebear.com/300/300" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. ynteger egestas magna a tristique consectetur. Vivamus volutpat porttitor elit. In efficitur felis nisi, eu eleifend sem tempor vitae. In laoreet lorem in diam ultricies lobortis. Sed viverra posuere tellus, lobortis finibus eros rhoncus vulputate..</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="https://placebear.com/300/300" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. ynteger egestas magna a tristique consectetur. Vivamus volutpat porttitor elit. In efficitur felis nisi, eu eleifend sem tempor vitae. In laoreet lorem in diam ultricies lobortis. Sed viverra posuere tellus, lobortis finibus eros rhoncus vulputate.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="https://placebear.com/300/300" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. ynteger egestas magna a tristique consectetur. Vivamus volutpat porttitor elit. In efficitur felis nisi, eu eleifend sem tempor vitae. In laoreet lorem in diam ultricies lobortis. Sed viverra posuere tellus, lobortis finibus eros rhoncus vulputate.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="https://placebear.com/300/300" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. ynteger egestas magna a tristique consectetur. Vivamus volutpat porttitor elit. In efficitur felis nisi, eu eleifend sem tempor vitae. In laoreet lorem in diam ultricies lobortis. Sed viverra posuere tellus, lobortis finibus eros rhoncus vulputate.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
			</section>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  </body>
</html>
